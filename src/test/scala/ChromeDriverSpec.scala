import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.htmlunit.HtmlUnitDriver
import org.scalatest._
import org.scalatest.selenium.{Chrome, WebBrowser}
import org.scalatest.selenium.WebBrowser.go

class ChromeDriverSpec extends FlatSpec with ShouldMatchers with Chrome {


  val host = "http://www.google.com/"

  "The blog app home page" should "have the correct title" in {
    go to (host + "index.html")
    pageTitle should be ("Google")
  }
}